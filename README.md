# HoLa-Data

[![Build Status](https://ci.gitlab.com/projects/7063/status.png?ref=master)](https://gitlab.com/holahanau/data/commits/master)

[![Play Store](https://developer.android.com/images/brand/en_generic_rgb_wo_60.png)](https://play.google.com/store/apps/details?id=de.nico.holavertretungen)

## Mitarbeiten

Wenn du beim Inhalt der App helfen willst, kannst du auf der
[Webseite](http://hola-gymnasium.de/) nach neuen PDF-Dateien suchen und
mich per [E-Mail](mailto:nicoalt@posteo.org) darüber informieren oder
einen [Issue](https://gitlab.com/holahanau/data/issues) auf GitLab
öffnen.

## Wofür ist dieses Projekt?

Die [offizielle Android App](https://gitlab.com/holahanau/android)
der Hohen Landesschule in Hanau bekommt ihre Daten aus diesem
Repository.

## TODO

- Data: [Issues](https://gitlab.com/holahanau/data/issues)
- Android: [Issues](https://gitlab.com/holahanau/android/issues)
- Upstream: [Issues](https://gitlab.com/asura/android/issues)

## Lizenz

Die `.json` Datei ist unter der MIT-Lizenz veröffentlicht. (Mehr
Informationen [hier](./LICENSE))

Der Inhalt der Pläne hingegen ist geistiges Eigentum der Hohen
Landesschule in Hanau und eventuell unter einer anderen Lizenz verfügbar.
